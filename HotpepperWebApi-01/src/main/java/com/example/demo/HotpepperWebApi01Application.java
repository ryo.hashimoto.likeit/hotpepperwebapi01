package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotpepperWebApi01Application {

	public static void main(String[] args) {
		SpringApplication.run(HotpepperWebApi01Application.class, args);
	}

}
