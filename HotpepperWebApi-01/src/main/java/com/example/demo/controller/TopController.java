package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Shop;
import com.example.demo.form.ShopForm;
import com.example.demo.repository.ShopRepository;


@Controller
public class TopController {

	@Autowired
	ShopRepository shoprepo;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index(
    		@ModelAttribute ShopForm shopForm,
    		ModelAndView model) {
        model.addObject("message", "Hello Springboot");

        List<Shop> shoplist = shoprepo.findAll();

		model.addObject("shoplist", shoplist);

		model.setViewName("index");
		return model;


    }


    @RequestMapping(value = "/area", method = RequestMethod.GET)
    public ModelAndView area(ModelAndView model) {
        model.addObject("message", "Hello test");

        return model;


    }




}
