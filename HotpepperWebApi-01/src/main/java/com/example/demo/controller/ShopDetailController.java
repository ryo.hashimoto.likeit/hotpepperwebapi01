package com.example.demo.controller;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Shop;
import com.example.demo.form.ShopForm;
import com.example.demo.repository.ShopRepository;

@Controller
public class ShopDetailController {
	@Autowired
	ShopRepository shoprepo;

	@ModelAttribute("shopForm")
    public ShopForm setupForm() {
		ShopForm ShopForm = new ShopForm();
        return ShopForm;
    }

    @RequestMapping(value = "/shopdetail", method = RequestMethod.GET)
    public ModelAndView index(
    		@ModelAttribute ShopForm shopForm,
    		ModelAndView model,
    		ServletRequest request
    		) {
    	int id = Integer.parseInt(request.getParameter("id"));
        Shop shopinfo = shoprepo.findById(id);

		model.addObject("shopinfo", shopinfo);

		model.setViewName("shopdetail");
		return model;


    }

}
