package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Shop;
import com.example.demo.form.ShopForm;
import com.example.demo.repository.ShopRepository;

@Controller
public class ApiController {
	@Autowired
	ShopRepository shoprepo;

	@ModelAttribute("shopForm")
    public ShopForm setupForm() {
		ShopForm ShopForm = new ShopForm();
        return ShopForm;
	}

    @RequestMapping(value = "/managementAPI", method = RequestMethod.GET)
    public ModelAndView hotpepper(
    		@ModelAttribute ShopForm shopForm,
    		ModelAndView model) {
        model.addObject("message", "Hello test");

        return model;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ModelAndView delete(ModelAndView model) {
    	shoprepo.deleteAll();
    	model.setViewName("managementAPI");
    	return model;

    }


	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	public ModelAndView submitComment(
			@ModelAttribute ShopForm shopForm,
//			@RequestParam("shopcount") int shopcount,
			BindingResult result,
			ModelAndView model
			) {

		// 取得したパラメータをもとに登録用データを作成
		Shop shops = new Shop();
		shops.setName(shopForm.getName());
		shops.setId(shopForm.getId());
		shops.setImage(shopForm.getImage());
		shops.setGenre(shopForm.getGenre());
		shops.setSmoking(shopForm.getSmoking());
		shops.setName_kana(shopForm.getName_kana());
		shops.setAccess(shopForm.getAccess());
		shops.setStation_name(shopForm.getStation_name());
		shops.setShop_catch(shopForm.getShop_catch());
		shops.setGenre_catch(shopForm.getGenre_catch());
		shops.setCapacity(shopForm.getCapacity());
		shops.setBudget_average(shopForm.getBudget_average());
		shops.setAddress(shopForm.getAddress());
		shops.setPhoto_pc_l(shopForm.getPhoto_pc_l());
//		shops.setOpen(shopForm.getOpen());
		shops.setClose(shopForm.getClose());
		shops.setSub_genre(shopForm.getSub_genre());

		// 登録
		shoprepo.save(shops);

//		model.addObject("shopcount",shopcount + "店舗の情報を取得しました。");

		// 遷移先のパスを指定(一覧表示のパス)
		model.setViewName("managementAPI");
		return model;
	}

}
