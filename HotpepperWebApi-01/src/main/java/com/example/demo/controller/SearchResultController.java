package com.example.demo.controller;

import java.util.List;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Shop;
import com.example.demo.form.ShopForm;
import com.example.demo.repository.ShopRepository;

@Controller
public class SearchResultController {
	@Autowired
	ShopRepository shoprepo;

	@ModelAttribute("shopForm")
    public ShopForm setupForm() {
		ShopForm ShopForm = new ShopForm();
        return ShopForm;
	}

    @RequestMapping(value = "/search_result", method = RequestMethod.POST)
    public ModelAndView search_result(
    		@ModelAttribute ShopForm shopForm,
    		ServletRequest request,
    		ModelAndView model) {

    	String search_word = request.getParameter("search_word");
    	List<Shop> result_shop = shoprepo.findBySerch_word(search_word);

    	model.addObject("result_shop", result_shop);
    	model.addObject("result_count", result_shop.size());

		// 画面描画用のテンプレート名を指定
		model.setViewName("search_result");

        return model;
    }

}
