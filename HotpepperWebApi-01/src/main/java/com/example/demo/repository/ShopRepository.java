package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Shop;

@Transactional
@Repository
public interface ShopRepository extends JpaRepository<Shop, Integer> {
	public Shop findById(int id);

	@Query(value="select * from shop where concat(name,genre_catch,shop_catch) like %:serch_word%",nativeQuery = true)
	public List<Shop> findBySerch_word(@Param("serch_word") String serch_word);

}
