package com.example.demo.form;

import java.io.Serializable;

public class ShopForm implements Serializable {

	private Integer id;
	private String name;
	private String image;
	private String genre;
	private String smoking;

	private String name_kana;
	private String address;
	private String station_name;
	private String genre_catch;
	private String shop_catch;
	private String capacity;
	private String budget_average;
	private String access;
	private String photo_pc_l;
	private String close;

	private String sub_genre;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getName_kana() {
		return name_kana;
	}
	public void setName_kana(String name_kana) {
		this.name_kana = name_kana;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStation_name() {
		return station_name;
	}
	public void setStation_name(String station_name) {
		this.station_name = station_name;
	}
	public String getGenre_catch() {
		return genre_catch;
	}
	public void setGenre_catch(String genre_catch) {
		this.genre_catch = genre_catch;
	}
	public String getShop_catch() {
		return shop_catch;
	}
	public void setShop_catch(String shop_catch) {
		this.shop_catch = shop_catch;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getBudget_average() {
		return budget_average;
	}
	public void setBudget_average(String budget_average) {
		this.budget_average = budget_average;
	}
	public String getAccess() {
		return access;
	}
	public void setAccess(String access) {
		this.access = access;
	}
	public String getPhoto_pc_l() {
		return photo_pc_l;
	}
	public void setPhoto_pc_l(String photo_pc_l) {
		this.photo_pc_l = photo_pc_l;
	}
	public String getClose() {
		return close;
	}
	public void setClose(String close) {
		this.close = close;
	}
	public String getSub_genre() {
		return sub_genre;
	}
	public void setSub_genre(String sub_genre) {
		this.sub_genre = sub_genre;
	}



}


