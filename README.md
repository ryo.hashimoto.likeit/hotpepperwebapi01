 **MySQL**
 
*  初期設定

<p>CREATE DATABASE hotpepper;</p>
<p>上記のデータベース作成以外特に初期設定の必要なし</p>


 **URL**


*  トップページ：http://localhost:8080/

<p>検索フォームは各店舗カードの見出しのワードを拾って検索</p>

*  WEB APIの情報取得管理ページ：http://localhost:8080/managementAPI

<p>「情報を取得」で店舗情報を取得し、DBにインサート（デフォルト５０件）</p>
<p>「データリセット」でDBに入ってるレコードを一括削除</p>

<p>API取得設定は「/HotpepperWebApi-01/src/main/resources/static/js/hotpepper.js」</p>

*  エリアコード取得ページ：http://localhost:8080/area

<p>「エリアを表示」で都内のエリアコード一覧を表示</p>
<p>hotpepper.jsの[middle_area]に上記のコードを参照して書き換えることで他の都内の店舗情報に変更可能（画面操作でできるようにする予定）</p>
